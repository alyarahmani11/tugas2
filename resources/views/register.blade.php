<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<div>
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>
	</div>
	
	<form action="/welcome" method="POST">
		@csrf
		<label for="first">First name:</label><br><br>
		<input type="text" name="nama_depan" id="first">
		<br><br>

		<label for="last">Last name:</label><br><br>
		<input type="text" name="nama_belakang" id="last">
		<br><br>

		<label>Gender:</label><br><br>
		<input type="radio" name="gender" value="M"> Male <br>
		<input type="radio" name="gender" value="F"> Female <br>
		<input type="radio" name="gender" value="O"> Other <br>
		<br>

		<label>Nationality:</label><br><br>
		<select name="Nationality">
			<option value="Indo">Indonesian</option>
			<option value="Sing">Singaporean</option>
			<option value="Malay">Malaysian</option>
			<option value="Aust">Australian</option>
		</select>
		<br><br>

		<label>Language Spoken:</label><br><br>
		<input type="checkbox" name="Bahasa" value="Indonesia">Bahasa Indonesia<br>
		<input type="checkbox" name="Bahasa" value="English">English<br>
		<input type="checkbox" name="Bahasa" value="Other">Other<br>
		<br><br>

		<label for="bio">Bio:</label><br><br>
		<textarea id="bio" cols="30" rows="10"></textarea><br>
		<input type="submit" value="Sign Up" href="/welcome">
	</form>
</body>
</html>